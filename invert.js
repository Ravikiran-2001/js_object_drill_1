function invert(object) {
  //created function
  if (Array.isArray(object) && typeof object !== object && object === null) {
    //checking input is object or not
    return "Invalid object";
  }
  const newObject = {}; //creating empty object
  for (let key in object) {
    //iterating in properties of object
    newObject[object[key]] = key; //replacing key with value and vice-versa.
  }
  return newObject;
}
module.exports = invert; //exporting function
