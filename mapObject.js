function mapObject(object, cb) {
  //creating function
  //created function
  if (Array.isArray(object) && typeof object !== object && object === null) {
    //checking input is object or not
    return "Invalid object";
  }
  const newObject = {}; //created empty object
  for (let key in object) {
    //iterating in properties of object
    newObject[key] = cb(object[key]); //calling cb function and storing value in new object.
  }
  return newObject;
}
module.exports = mapObject; //exportng function
