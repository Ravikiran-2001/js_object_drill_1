function values(object) {
  //created function
  if (Array.isArray(object) && typeof object !== object && object === null) {
    //checking input is object or not
    return "Invalid object";
  }
  let array = []; //created array
  for (let key in object) {
    //iterating inside object
    array.push(object[key]); //pushing value inside array.
  }
  return array;
}
module.exports = values; //exporting function
