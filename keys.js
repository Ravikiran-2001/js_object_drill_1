function keys(object) {
  //created function
  if (Array.isArray(object) && typeof object !== object && object === null) {
    //checking input is object or not
    return "Invalid object";
  }
  let array = []; //created empty array
  for (let keys in object) {
    //iterating in properties of object
    array.push(keys); //pushing keys inside empty array.
  }
  return array;
}
module.exports = keys; //exporting function
