function pairs(object) {
  //created function
  if (Array.isArray(object) && typeof object !== object && object === null) {
    //checking input is object or not
    return "Invalid object";
  }
  let array = []; //created empty array
  for (let key in object) {
    //iterating inside object
    array.push([key, object[key]]); //pushing key and value inside array.
  }
  return array;
}
module.exports = pairs; //exporting function
