function defaults(object, defaultObject) {
  //creating
  if (isObject(object) && isObject(defaultObject)) {
    //checking given input is object or not
    return "Invalid object";
  }
  for (let key in defaultObject) {
    //iterating inside defaultObject
    if (object.hasOwnProperty(key)) {
      //checking whether key is present in object or not
      if (object[key] === undefined) {
        //if value is undefined ,value of defaultObject will be assigned to object
        object[key] = defaultObject[key];
      }
    }
  }
  return object;
}
module.exports = defaults; //importing function

function isObject(object) {
  //created function to check given input is object or not
  return !Array.isArray(object) && typeof object === object && object !== null;
}
