const mapObject = require("../mapObject"); //exporting function
const object = {
  //creating object
  age1: 10,
  age2: 15,
  age3: 20,
};
function callBack(value) {
  //created callBack function
  return value + 2;
}
console.log(mapObject(object, callBack)); //calling function and printing returned value
