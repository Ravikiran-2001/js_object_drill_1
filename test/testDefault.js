const defaults = require("../defaults"); //importing function
const object = {
  //created object
  id1: 12,
  id2: 21,
  id3: 16,
  id4: undefined,
};
const defaultObject = {
  //created default object
  id2: 44,
  id4: 67,
  id6: 123,
  id8: 43,
};

console.log(defaults(object, defaultObject)); //calling the function and printing returned object
